PATH=$PATH:/usr/local/bin
export PATH

cd /home/spot1/turismo/turismo/spiders/


scrapy runspider chileviajes_cl.py
scrapy runspider despegar_cl.py
scrapy runspider cocha_com.py
#scrapy runspider json_despegar.py

echo 'Archivos csv generados por script del mismo nombre sin la palabra output_ y extension .csv' >> errores.csv


#Se crea una carpeta donde se alamacenaran los archivos csv exportadas por cada pagina
#mkdir /home/spot1/outputfiles/`date +%Y%m%d_%H`_d

for entry in $(ls output_json*)
do
   if [ -s ${entry} ];then
       echo archivo correcto: ${entry}
       #Enviamos los datos a S3
       aws s3 cp ${entry} s3://spot-backup/spot-turismo/json/`date +%Y%m%d_%H`_d/
       aws s3 mv ${entry} s3://spot-turismo/json/`date +%Y%m%d_%H`_d/
       rm ${entry}

   else
       echo archivo vacío : ${entry}
       echo  ${entry} >> errores.csv
   fi
done

#Se ejecuta solo el json
scrapy runspider json_despegar.py
#Recorremos todos los archivos que empiecen con la palabra output_...
for entry in $(ls output_*)
do
   if [ -s ${entry} ];then
       echo archivo correcto: ${entry}
       #Enviamos los datos a S3
       aws s3 cp ${entry} s3://spot-backup/spot-turismo/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d/
       aws s3 mv ${entry} s3://spot-turismo/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d/

   else
       echo archivo vacío : ${entry}
       echo  ${entry} >> errores.csv
   fi
done

#Se sube archivo general  output.csv
#aws s3 mv output.csv s3://spot-turismo/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d.csv

#Se sube archivo errores
aws s3 cp errores.csv s3://spot-backup/spot-turismo/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv
aws s3 mv errores.csv s3://spot-turismo/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv

#Se eliminan archivos .pyc y .csv
cd /home/spot1/turismo
find . -name \*.pyc -delete
find . -name \*.csv -delete

cd /home/spot1/cronlog
aws s3 cp bash_`date +%Y%m%d_%H`_d.log  s3://spot-backup/spot-turismo/descargasScrapy/bash_run/bash_`date +%Y%m%d_%H`_d.log
aws s3 mv bash_`date +%Y%m%d_%H`_d.log  s3://spot-turismo/descargasScrapy/bash_run/bash_`date +%Y%m%d_%H`_d.log
find . -name \*.log -delete


