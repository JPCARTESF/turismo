# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, Compose, Join, MapCompose, TakeFirst
from w3lib.html import remove_tags
import scrapy


class TurismoItem(scrapy.Item):
	###################################
	######## VARIABLES CHILEESTUYO #######
	###################################	
	id_producto = scrapy.Field()
	nombre = scrapy.Field()
	categoria = scrapy.Field()
	detalles_precio = scrapy.Field()
	tipo_producto = scrapy.Field()
	url = scrapy.Field()
	fecha_extraccion = scrapy.Field()
	hora_extraccion = scrapy.Field()
	fuente = scrapy.Field()
	url_imagen = scrapy.Field()
	latitud = scrapy.Field()
	longitud= scrapy.Field()
	telefono = scrapy.Field()
	direccion = scrapy.Field()
	servicio_turistico = scrapy.Field()

	###################################
	######## VARIABLES COCHA #######
	###################################	
	hotel_id = scrapy.Field()
	nombre_hotel = scrapy.Field()
			#Origen
	direccion_origen = scrapy.Field()
	ciudad_origen = scrapy.Field()
	pais_origen = scrapy.Field()	
	codigo_pais_origen = scrapy.Field()
	codigo_aero_origen = scrapy.Field()

			#Destino
	direccion_destino = scrapy.Field()
	ciudad_destino = scrapy.Field()
	pais_destino = scrapy.Field()	
	codigo_pais_destino = scrapy.Field()
	codigo_aero_destino = scrapy.Field()
	tarifa_alta = scrapy.Field()
	tarifa_baja  = scrapy.Field() 
	codigo_tarifa = scrapy.Field()
	latitud = scrapy.Field()
	longitud = scrapy.Field()
	
	precio = scrapy.Field()
	descuento_moneda = scrapy.Field()
	precio_oferta_moneda = scrapy.Field()
	precio_sin_oferta_moneda = scrapy.Field()

	precio_oferta_clp = scrapy.Field()
	precio_sin_oferta_clp = scrapy.Field()
	hotel_rating = scrapy.Field()
	hotel_rating_display = scrapy.Field()
	exchangeRate = scrapy.Field()
	id_publicaciones = scrapy.Field()

	###################################
	######## VARIABLES DESPEGAR #######
	###################################	
	id_hotel = scrapy.Field()
	nombre_hotel = scrapy.Field()
	stars_hotel = scrapy.Field()
	fecha_checkin = scrapy.Field()
	fecha_checkout = scrapy.Field()
	hora_checkin = scrapy.Field()
	hora_checkout = scrapy.Field()
	ciudad_hotel = scrapy.Field()
	iata = scrapy.Field()
	latitude_hotel = scrapy.Field()
	longitude_hotel = scrapy.Field()
	direccion_hotel = scrapy.Field()
	rooms = scrapy.Field()
	noches = scrapy.Field()
	num_noches = scrapy.Field()
	rooms_pack = scrapy.Field()
	id_vuelo = scrapy.Field()
	duracion_vuelo_ida = scrapy.Field()
	escalas_ida = scrapy.Field()
	num_escalas_id = scrapy.Field()
	duracion_vuelo_vuelta = scrapy.Field()
	escalas_vuelta = scrapy.Field()
	num_escalas_vuelta = scrapy.Field()
	id_asistencia_viajes = scrapy.Field()
	descripcion_asistencia_viajes = scrapy.Field()
	anuncio_asistencia_viajes = scrapy.Field()
	codigo_moneda_clp = scrapy.Field()
	precio_anunciado_total_clp = scrapy.Field()
	precio_anunciado_bruto_clp = scrapy.Field()
	precio_anunciado_por_clp = scrapy.Field()
	anunciado_cargos = scrapy.Field()
	descuento_clp = scrapy.Field()
	total_clp = scrapy.Field()
	total_cargos_clp = scrapy.Field()
	breakdown_base_clp = scrapy.Field()
	breakdown_base_por_clp = scrapy.Field()
	breakdown_cargos_clp = scrapy.Field()
	breakdown_cargos_por = scrapy.Field()
	codigo_moneda_usd = scrapy.Field()
	precio_anunciado_total_usd = scrapy.Field()
	precio_anunciado_bruto_usd = scrapy.Field()
	precio_anunciado_por_usd = scrapy.Field()
	anunciado_cargos_usd = scrapy.Field()
	descuento_usd = scrapy.Field()
	total_usd = scrapy.Field()
	total_cargos_usd = scrapy.Field()
	breakdown_base_usd = scrapy.Field()
	breakdown_base_por_usd = scrapy.Field()
	breakdown_cargos_usd = scrapy.Field()
	breakdown_cargos_por_usd = scrapy.Field()
	iata_destino = scrapy.Field()
	gid_destino = scrapy.Field()
	nombre_destino = scrapy.Field()
	latitude_destino = scrapy.Field()
	longitude_destino = scrapy.Field()
	iata_origen = scrapy.Field()
	gid_origen = scrapy.Field()
	nombre_origen = scrapy.Field()
	nombre_aerolinea = scrapy.Field()
	score_general_aerolinea = scrapy.Field()
	points_aerolinea = scrapy.Field()
	fuente = scrapy.Field()
	id_aerolinea = scrapy.Field()
	ciudad_destino= scrapy.Field()
	pais_destino= scrapy.Field()
	ciudad_origen= scrapy.Field()
	pais_origen	= scrapy.Field()

	json_data = scrapy.Field()
	url_json = scrapy.Field()
	url_view = scrapy.Field()





class ChileestuyoLoader(ItemLoader):
	default_item_class = TurismoItem

	def strip_dashes(value):
		if value:
			return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','').replace('.','')
	def strip_others(value):
		if value:
			return value.replace('\t','').replace('\n','').replace('\r','')

	categoria_in  = Compose(Join(),strip_dashes)
	detalles_precio_in = Compose(Join(),strip_dashes)
	servicio_turistico_in = Compose(Join(), strip_dashes)
	tipo_producto_in = Compose(Join(),strip_dashes)
	nombre_in = Compose(Join(),strip_dashes)


	id_producto_out = TakeFirst()
	nombre_out = TakeFirst()
	#categoria_out
	detalles_precio_out = TakeFirst()
	tipo_producto_out = TakeFirst()
	url_out = TakeFirst()
	fecha_extraccion_out = TakeFirst()
	hora_extraccion_out = TakeFirst()
	fuente_out= TakeFirst()
	url_imagen_out = TakeFirst()
	latitud_out = TakeFirst()
	longitu_out = TakeFirst()
	telefono_out = TakeFirst()
	direccion_out= TakeFirst()
   #servicio_turistico_out = TakeFirst()

class CochaLoader(ItemLoader):
	default_item_class = TurismoItem

	def strip_dashes(value):
		if value:
			return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','').replace('.','')
	def strip_others(value):
		if value:
			return value.replace('\t','').replace('\n','').replace('\r','')

	hotel_id_out = TakeFirst()
	nombre_hotel_out = TakeFirst()
			#Origen
	direccion_origen_out = TakeFirst()
	ciudad_origen_out = TakeFirst()
	pais_origen_out = TakeFirst()	
	codigo_pais_origen_out = TakeFirst()
	codigo_aero_origen_out = TakeFirst()

			#Destino
	direccion_destino_out = TakeFirst()
	ciudad_destino_out = TakeFirst()
	pais_destino_out = TakeFirst()	
	codigo_pais_destino_out = TakeFirst()
	codigo_aero_destino_out = TakeFirst()
	tarifa_alta_out = TakeFirst()
	tarifa_baja_out = TakeFirst() 
	codigo_tarifa_out = TakeFirst()
	latitud_out = TakeFirst()
	longitud_out = TakeFirst()
	
	precio_out = TakeFirst()
	descuento_moneda_out = TakeFirst()
	precio_oferta_moneda_out = TakeFirst()
	precio_sin_oferta_moneda_out = TakeFirst()

	precio_oferta_clp_out = TakeFirst()
	precio_sin_oferta_clp_out = TakeFirst()
	hotel_rating_out = TakeFirst()
	hotel_rating_display_out = TakeFirst()
	exchangeRate_out = TakeFirst()
	id_publicaciones_out = TakeFirst()

	fecha_checkin_out = TakeFirst()	
	fecha_checkout_out = TakeFirst()	
	fuente_out = TakeFirst()			
	fecha_extraccion_out = TakeFirst()
	hora_extraccion_out = TakeFirst()	


class DespegarLoader(ItemLoader):
	default_item_class= TurismoItem	

	id_hotel_out = TakeFirst()
	nombre_hotel_out = TakeFirst()
	stars_hotel_out = TakeFirst()
	fecha_checkin_out = TakeFirst()
	fecha_checkout_out = TakeFirst()
	hora_checkin_out = TakeFirst()
	hora_checkout_out = TakeFirst()
	ciudad_hotel_out = TakeFirst()
	iata_out = TakeFirst()
	latitude_hotel_out = TakeFirst()
	longitude_hotel_out = TakeFirst()
	direccion_hotel_out = TakeFirst()
	rooms_out = TakeFirst()
	noches_out = TakeFirst()
	num_noches_out = TakeFirst()
	rooms_pack_out = TakeFirst()
	id_vuelo_out = TakeFirst()
	duracion_vuelo_ida_out = TakeFirst()
	escalas_ida_out = TakeFirst()
	num_escalas_id_out = TakeFirst()
	duracion_vuelo_vuelta_out = TakeFirst()
	escalas_vuelta_out = TakeFirst()
	num_escalas_vuelta_out = TakeFirst()
	id_asistencia_viajes_out = TakeFirst()
	descripcion_asistencia_viajes_out = TakeFirst()
	anuncio_asistencia_viajes_out = TakeFirst()
	codigo_moneda_clp_out = TakeFirst()
	precio_anunciado_total_clp_out = TakeFirst()
	precio_anunciado_bruto_clp_out = TakeFirst()
	precio_anunciado_por_clp_out = TakeFirst()
	anunciado_cargos_out = TakeFirst()
	descuento_clp_out = TakeFirst()
	total_clp_out = TakeFirst()
	total_cargos_clp_out = TakeFirst()
	breakdown_base_clp_out = TakeFirst()
	breakdown_base_por_clp_out = TakeFirst()
	breakdown_cargos_clp_out = TakeFirst()
	breakdown_cargos_por_out = TakeFirst()
	codigo_moneda_usd_out = TakeFirst()
	precio_anunciado_total_usd_out = TakeFirst()
	precio_anunciado_bruto_usd_out = TakeFirst()
	precio_anunciado_por_usd_out = TakeFirst()
	anunciado_cargos_usd_out = TakeFirst()
	descuento_usd_out = TakeFirst()
	total_usd_out = TakeFirst()
	total_cargos_usd_out = TakeFirst()
	breakdown_base_usd_out = TakeFirst()
	breakdown_base_por_usd_out = TakeFirst()
	breakdown_cargos_usd_out = TakeFirst()
	breakdown_cargos_por_usd_out = TakeFirst()
	iata_destino_out = TakeFirst()
	gid_destino_out = TakeFirst()
	nombre_destino_out = TakeFirst()
	latitude_destino_out = TakeFirst()
	longitude_destino_out = TakeFirst()
	iata_origen_out = TakeFirst()
	gid_origen_out = TakeFirst()
	nombre_origen_out = TakeFirst()
	nombre_aerolinea_out = TakeFirst()
	score_general_aerolinea_out = TakeFirst()
	points_aerolinea_out = TakeFirst()
	fuente_out = TakeFirst()
	id_aerolinea_out = TakeFirst()
	ciudad_destino_out = TakeFirst()
	pais_destino_out = TakeFirst()
	ciudad_origen_out = TakeFirst()
	pais_origen_out = TakeFirst()
	fecha_extraccion_out =TakeFirst()
	hora_extraccion_out  = TakeFirst()

class json_loader(ItemLoader):
	default_item_class= TurismoItem	

	#Contiene el json
	json_data_out = TakeFirst()

	iata_origen_out = TakeFirst()
	iata_destino_out = TakeFirst()
	fecha_checkin_out = TakeFirst()
	fecha_checkout_out =TakeFirst()

	fecha_extraccion_out = TakeFirst()
	hora_extraccion_out = TakeFirst()
	url_json_out = TakeFirst()
	url_view_out = TakeFirst()
	fuente_out =TakeFirst()



