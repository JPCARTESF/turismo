# -*- coding: utf-8 -*-
from turismo import items
from turismo import settings
from turismo.items import *
import scrapy
import csv

class TurismoPipeline(object):
	def __init__(self):
		self.file_name = {} 
		self.keys_chile = {'id_producto','nombre','categoria','detalles_precio','tipo_producto','url','fecha_extraccion','hora_extraccion','fuente','url_imagen','telefono','direccion','servicio_turistico'}
		self.keys_cocha = {'hotel_id','nombre_hotel','direccion_origen','ciudad_origen','pais_origen',  'codigo_pais_origen','codigo_aero_origen','direccion_destino','ciudad_destino','pais_destino',  'codigo_pais_destino','codigo_aero_destino','tarifa_alta','tarifa_baja', 'codigo_tarifa','latitud','longitud','precio','descuento_moneda','precio_oferta_moneda','precio_sin_oferta_moneda','precio_oferta_clp','precio_sin_oferta_clp','hotel_rating','hotel_rating_display','exchangeRate','id_publicaciones','fecha_checkin','fecha_checkout','fuente','fecha_extraccion','hora_extraccion'}
		self.keys_despegar = {'ciudad_destino','pais_destino','ciudad_origen','pais_origen','iata_origen','gid_origen','nombre_origen','iata_destino','gid_destino','nombre_destino','latitude_destino','longitude_destino','id_hotel','nombre_hotel','stars_hotel','fecha_checkin','fecha_checkout','hora_checkin','hora_checkout','ciudad_hotel','latitude_hotel','longitude_hotel','direccion_hotel','rooms','noches','num_noches','rooms_pack','codigo_moneda_clp','precio_anunciado_total_clp','precio_anunciado_bruto_clp','precio_anunciado_por_clp','anunciado_cargos','descuento_clp','total_clp','total_cargos_clp','breakdown_base_clp','breakdown_base_por_clp','breakdown_cargos_clp','breakdown_cargos_por','codigo_moneda_usd','precio_anunciado_total_usd','precio_anunciado_bruto_usd','precio_anunciado_por_usd','anunciado_cargos_usd','descuento_usd','total_usd','total_cargos_usd','breakdown_base_usd','breakdown_base_por_usd','breakdown_cargos_usd','breakdown_cargos_por_usd','duracion_vuelo_ida','escalas_ida','num_escalas_id','duracion_vuelo_vuelta','escalas_vuelta','num_escalas_vuelta','id_asistencia_viajes','descripcion_asistencia_viajes','anuncio_asistencia_viajes','id_vuelo','id_aerolinea','nombre_aerolinea','score_general_aerolinea','points_aerolinea','fuente','fecha_extraccion','hora_extraccion'}
	def open_spider(self, spider):

		self.file_name=csv.writer(open('output_'+spider.name+'.csv','w'))
		if spider.name.find('chile')!=-1:
			self.file_name.writerow(['id_producto','nombre','categoria','detalles_precio','tipo_producto','url','fecha_extraccion','hora_extraccion','fuente','url_imagen','telefono','direccion','servicio_turistico'])
		elif spider.name.find('cocha')!=-1:
			self.file_name.writerow(['hotel_id','nombre_hotel','direccion_origen','ciudad_origen','pais_origen',    'codigo_pais_origen','codigo_aero_origen','direccion_destino','ciudad_destino','pais_destino',  'codigo_pais_destino','codigo_aero_destino','tarifa_alta','tarifa_baja', 'codigo_tarifa','latitud','longitud','precio','descuento_moneda','precio_oferta_moneda','precio_sin_oferta_moneda','precio_oferta_clp','precio_sin_oferta_clp','hotel_rating','hotel_rating_display','exchangeRate','id_publicaciones','fecha_checkin','fecha_checkout','fuente','fecha_extraccion','hora_extraccion'])
		elif spider.name.find('despegar_cl')!=-1:
			self.file_name.writerow(['ciudad_destino','pais_destino','ciudad_origen','pais_origen','iata_origen','gid_origen','nombre_origen','iata_destino','gid_destino','nombre_destino','latitude_destino','longitude_destino','id_hotel','nombre_hotel','stars_hotel','fecha_checkin','fecha_checkout','hora_checkin','hora_checkout','ciudad_hotel','latitude_hotel','longitude_hotel','direccion_hotel','rooms','noches','num_noches','rooms_pack','codigo_moneda_clp','precio_anunciado_total_clp','precio_anunciado_bruto_clp','precio_anunciado_por_clp','anunciado_cargos','descuento_clp','total_clp','total_cargos_clp','breakdown_base_clp','breakdown_base_por_clp','breakdown_cargos_clp','breakdown_cargos_por','codigo_moneda_usd','precio_anunciado_total_usd','precio_anunciado_bruto_usd','precio_anunciado_por_usd','anunciado_cargos_usd','descuento_usd','total_usd','total_cargos_usd','breakdown_base_usd','breakdown_base_por_usd','breakdown_cargos_usd','breakdown_cargos_por_usd','duracion_vuelo_ida','escalas_ida','num_escalas_id','duracion_vuelo_vuelta','escalas_vuelta','num_escalas_vuelta','id_asistencia_viajes','descripcion_asistencia_viajes','anuncio_asistencia_viajes','id_vuelo','id_aerolinea','nombre_aerolinea','score_general_aerolinea','points_aerolinea','fuente','fecha_extraccion','hora_extraccion'])
		elif spider.name.find('json_despegar')!=-1:
			self.file_name.writerow(['json_data','iata_origen','iata_destino','fecha_checkin','fecha_checkout','url_json','url_view','fuente','fecha_extraccion','hora_extraccion'])

	def process_item(self, item, spider):
		if spider.name.find('chile')!=-1:
			for key in self.keys_chile:
				item.setdefault(key,'NA')

			self.file_name.writerow([
									item['id_producto'],
									item['nombre'],
									item['categoria'],
									item['detalles_precio'],
									item['tipo_producto'],
									item['url'],
									item['fecha_extraccion'],
									item['hora_extraccion'],
									item['fuente'],
									item['url_imagen'],
									#item['latitud'],
									#item['longitud'],
									item['telefono'],
									item['direccion'],
									item['servicio_turistico']
								])
			return item
		elif spider.name.find('cocha')!=-1:
			for key in self.keys_cocha:
				item.setdefault(key,'NA')
			self.file_name.writerow([
									item['hotel_id'],
									item['nombre_hotel'],
									item['direccion_origen'],
									item['ciudad_origen'],
									item['pais_origen'],
									item['codigo_pais_origen'],
									item['codigo_aero_origen'],
									item['direccion_destino'],
									item['ciudad_destino'],
									item['pais_destino'],
									item['codigo_pais_destino'],
									item['codigo_aero_destino'],
									item['tarifa_alta'],
									item['tarifa_baja'],
									item['codigo_tarifa'],
									item['latitud'],
									item['longitud'],
									item['precio'],
									item['descuento_moneda'],
									item['precio_oferta_moneda'],
									item['precio_sin_oferta_moneda'],
									item['precio_oferta_clp'],
									item['precio_sin_oferta_clp'],
									item['hotel_rating'],
									item['hotel_rating_display'],
									item['exchangeRate'],
									item['id_publicaciones'],
									item['fecha_checkin'],
									item['fecha_checkout'],
									item['fuente'],
									item['fecha_extraccion'],
									item['hora_extraccion'],
									])
			return item
		elif spider.name.find('despegar_cl')!=-1:
			for key in self.keys_despegar:
				item.setdefault(key,'NA')
			self.file_name.writerow([
									 item['ciudad_destino']
									,item['pais_destino']
									,item['ciudad_origen']
									,item['pais_origen']
									,item['iata_origen']
									,item['gid_origen']
									,item['nombre_origen']
									,item['iata_destino']
									,item['gid_destino']
									,item['nombre_destino']
									,item['latitude_destino']
									,item['longitude_destino']
									,item['id_hotel']
									,item['nombre_hotel']
									,item['stars_hotel']
									,item['fecha_checkin']
									,item['fecha_checkout']
									,item['hora_checkin']
									,item['hora_checkout']
									,item['ciudad_hotel']
									,item['latitude_hotel']
									,item['longitude_hotel']
									,item['direccion_hotel']
									,item['rooms']
									,item['noches']
									,item['num_noches']
									,item['rooms_pack']
									,item['codigo_moneda_clp']
									,item['precio_anunciado_total_clp']
									,item['precio_anunciado_bruto_clp']
									,item['precio_anunciado_por_clp']
									,item['anunciado_cargos']
									,item['descuento_clp']
									,item['total_clp']
									,item['total_cargos_clp']
									,item['breakdown_base_clp']
									,item['breakdown_base_por_clp']
									,item['breakdown_cargos_clp']
									,item['breakdown_cargos_por']
									,item['codigo_moneda_usd']
									,item['precio_anunciado_total_usd']
									,item['precio_anunciado_bruto_usd']
									,item['precio_anunciado_por_usd']
									,item['anunciado_cargos_usd']
									,item['descuento_usd']
									,item['total_usd']
									,item['total_cargos_usd']
									,item['breakdown_base_usd']
									,item['breakdown_base_por_usd']
									,item['breakdown_cargos_usd']
									,item['breakdown_cargos_por_usd']

									,item['duracion_vuelo_ida']
									,item['escalas_ida']
									,item['num_escalas_id']
									,item['duracion_vuelo_vuelta']
									,item['escalas_vuelta']
									,item['num_escalas_vuelta']
									,item['id_asistencia_viajes']
									,item['descripcion_asistencia_viajes']
									,item['anuncio_asistencia_viajes']

									,item['id_vuelo']
									,item['id_aerolinea']
									,item['nombre_aerolinea']
									,item['score_general_aerolinea']
									,item['points_aerolinea']
									,item['fuente']
									,item['fecha_extraccion']
									,item['hora_extraccion']
								])
			return item

		elif spider.name.find('json_despegar')!=-1:
			for key in self.keys_despegar:
				item.setdefault(key,'NA')

			self.file_name.writerow([
								item['json_data'],
								item['iata_origen'],
								item['iata_destino'],
								item['fecha_checkin'],
								item['fecha_checkout'],
								item['url_json'],
								item['url_view'],
								item['fuente'],
								item['fecha_extraccion'],
								item['hora_extraccion'],
								])
			return item







