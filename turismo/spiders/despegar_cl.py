# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request,FormRequest
from turismo.items import DespegarLoader
from datetime import datetime, date, time, timedelta
import time
import json
from w3lib.url import url_query_parameter


class despegarSpider(CrawlSpider):
	name = 'despegar_cl'
	#allowed_domains = ['despegar.cl']
	#start_urls= ['https://www.despegar.cl/shapi/packages?page=1&pageSize=1000&back=2017-09-16&departure=2017-09-04&from=CIT_6624&to=CIT_4088&distribution=2&locale=es-CL']
	#start_urls= ['https://www.despegar.cl/shapi/packages?page=1&pageSize=1000&back=2017-09-16&departure=2017-09-04&from=CIT_6624&to=CIT_5543&distribution=2&locale=es-CL']

	IATA_CHILE ={}
	IATA_CHILE ['ARI'] = ['Aeropuerto Internacional Chacalluta','Arica',' Arica y Parinacota',' Chile','CIT_349']
	IATA_CHILE ['CCP'] = ['Aeropuerto Carriel Sur','Concepción',' Biobío',' Chile','CIT_1162']
	IATA_CHILE ['IPC'] = ['Aeropuerto Internacional Mataveri','Isla de Pascua',' Valparaíso',' Chile','CIT_3164']
	IATA_CHILE ['PUQ'] = ['Aeropuerto Internacional Presidente Carlos Ibáñez del Campo','Punta Arenas',' Magallanes y Antártica Chilena',' Chile','CIT_5970']
	IATA_CHILE ['SCL'] = ['Aeropuerto Internacional Comodoro Arturo Merino Benítez','Santiago de Chile',' Metropolitana de Santiago',' Chile','CIT_6624']
	#IATA_CHILE ['QRC'] = ['Aeródromo de la Independencia','Rancagua',' OHiggins',' Chile','CIT_9324']
	IATA_CHILE ['ZCO'] = ['Aeropuerto Internacional La Araucanía','Freire (Temuco)','Chile','La Araucanía','AIR_827764']
	IATA_CHILE ['IQQ'] = ['Aeropuerto Internacional Diego Aracena','Iquique','Tarapacá','Chile','AIR_194666']
	IATA_CHILE ['PMC'] = ['Aeropuerto Internacional El Tepual','Puerto Montt','Los Lagos','Chile','AIR_197162']


	IATA_DESTINOS = {}		

	IATA_DESTINOS ['XX1'] =	['Aeropuerto Internacional Carrasco', 'Montevideo','Montevideo','Uruguay','CIT_4844']
	IATA_DESTINOS ['XX2'] =	['Aeropuerto Internacional Jose Marti','La Habana','La Habana','Cuba','AIR_1344841']
	IATA_DESTINOS ['XX3'] =	['Aeropuerto Internacional Cancún','Cancún','Cancún','México','AIR_193176']
	IATA_DESTINOS ['XX4'] =	['NA','Asunción','Central','Paraguay','CITY_387']
	IATA_DESTINOS ['XX5'] =	['Aeropuerto Pinto Martins','Fortaleza','Fortaleza','Brasil','AIR_193857']
	IATA_DESTINOS ['XX6'] =	['Aeropuerto Salgado Filho', 'Porto Alegre','Porto Alegre','Brasil','AIR_197203']
	IATA_DESTINOS ['XX7'] =	['Aeropuerto Santos Dumont','Río de Janeiro','Río de Janeiro','Brasil','AIR_197551']
	IATA_DESTINOS ['XX8'] =	['NA','Montañita','Santa Elena','Ecuador','CIT_34078']
	IATA_DESTINOS ['XX9'] =	['Aeropuerto Internacional Capitan Corbeta CA Curbelo','Punta del Este','Punta del Este','Uruguay','AIR_197009']
	IATA_DESTINOS ['X10'] = ['NA','Punta Cana', 'La Altagracia', 'República Dominicana','CIT_5963']
	IATA_DESTINOS ['AEP'] = ['Aeroparque Jorge Newbery','Buenos Aires',' Buenos Aires',' Argentina','CIT_982']
	IATA_DESTINOS ['AFA'] = ['Aeropuerto Internacional Suboficial Ayudante Santiago Germano','San Rafael',' Mendoza',' Argentina','CIT_102']
	IATA_DESTINOS ['AOL'] = ['Aeropuerto Internacional de Paso de los Libres','Paso de los Libres',' Córdoba',' Argentina','CIT_299']
	#IATA_DESTINOS ['ASU'] = ['Aeropuerto Internacional Silvio Pettirossi','Luque',' Central',' Paraguay']
	IATA_DESTINOS ['ATH'] = ['Aeropuerto Internacional Eleftherios Venizelos','Atenas',' Ática',' Grecia','AIR_192061']
	IATA_DESTINOS ['AUA'] = ['Aeropuerto Internacional Reina Beatrix','Oranjestad',' Aruba',' Países Bajos','CIT_420']
	IATA_DESTINOS ['BCN'] = ['Aeropuerto de Barcelona-El Prat','Barcelona',' Cataluña',' España','CIT_576']
	IATA_DESTINOS ['BIN'] = ['Aeropuerto de Bamiyán','Bamiyán',' Provincia de Bamiyán',' Afganistán','CIT_714']
	IATA_DESTINOS ['BOD'] = ['Aeropuerto de Burdeos-Mérignac','Burdeos',' Aquitania',' Francia','CIT_852']
	IATA_DESTINOS ['BOG'] = ['Aeropuerto Internacional El Dorado','Bogotá',' Bogotá',' Colombia','AIR_192510']
	#IATA_DESTINOS ['BPM'] = ['Base Aérea de Bagram','Bagram',' Provincia de Parwan',' Afganistán']
	IATA_DESTINOS ['BSL'] = ['Aeropuerto de Basilea-Mulhouse-Friburgo','Basilea, Mulhouse, Friburgo',' Alsacia',' Francia','CIT_936']
	#IATA_DESTINOS ['BST'] = ['Aeropuerto de Bost','Lashkar Gah',' Provincia de Helmand',' Afganistán']
	IATA_DESTINOS ['BUD'] = ['Aeropuerto de Budapest-Ferenc Liszt','Budapest',' Budapest',' Hungría','CIT_981']
	IATA_DESTINOS ['CDG'] = ['Aeropuerto de París-Charles de Gaulle','París',' Isla de Francia',' Francia','CIT_5543']
	IATA_DESTINOS ['CGN'] = ['Aeropuerto Internacional de Colonia/Bonn Konrad Adenauer','Colonia, Bonn',' Renania del Norte-Westfalia',' Alemania','CIT_1250']
	IATA_DESTINOS ['CLX'] = ['Aeropuerto Clorinda','Clorinda',' Formosa',' Argentina','CIT_1366']
	#IATA_DESTINOS ['CNQ'] = ['Aeropuerto Internacional Doctor Fernándo Piragine Niveyro','Ciudad de Corrientes',' Corrientes',' Argentina']
	IATA_DESTINOS ['COC'] = ['Aeropuerto Comodoro Pierrestegui','Concordia',' Entre Ríos',' Argentina','CIT_1431']
	IATA_DESTINOS ['CTC'] = ['Aeropuerto Coronel Felipe Varela','San Fernando del Valle de Catamarca',' Catamarca',' Argentina','CIT_1532']
	IATA_DESTINOS ['CCS'] = ['Aeropuerto Internacional de Maiquetía Simón Bolívar','Caracas',' Vargas',' Venezuela','CIT_1165']
	IATA_DESTINOS ['DAY'] = ['Aeropuerto Internacional James M. Cox-Dayton','Dayton',' Ohio',' Estados Unidos','CIT_1674']#REVISAR EJEMPLOS
	#IATA_DESTINOS ['DJE'] = ['Aeropuerto Internacional de Yerba-Zarzis','Yerba',' Medenine',' Túnez']
	IATA_DESTINOS ['DME'] = ['Aeropuerto Internacional de Moscú-Domodédovo','Moscú',' Óblast de Moscú',' Rusia','CIT_4700']
	IATA_DESTINOS ['DXB'] = ['Aeropuerto Internacional de Dubái','Dubái',' Dubái',' Emiratos Árabes Unidos','CIT_1899']
	#IATA_DESTINOS ['EAP'] = ['Aeropuerto de Basilea-Mulhouse-Friburgo','Basilea, Mulhouse, Friburgo',' Alsacia',' Francia'] REPETIDO? CON OTRO CODIGO
	IATA_DESTINOS ['EDI'] = ['Aeropuerto de Edimburgo','Edimburgo',' Escocia',' Reino Unido','CIT_1947']
	IATA_DESTINOS ['EGE'] = ['Aeropuerto Regional del Condado de Eagle','Eagle-Vail',' Colorado',' Estados Unidos','CIT_1967']
	IATA_DESTINOS ['EIN'] = ['Aeropuerto de Eindhoven','Eindhoven',' Brabante Septentrional',' Países Bajos','CIT_1985']
	IATA_DESTINOS ['EZE'] = ['Aeropuerto Internacional Ministro Pistarini','Ezeiza',' Buenos Aires',' Argentina','CIT_34089']
#	IATA_DESTINOS ['FBD'] = ['Aeropuerto de Fayzabad','Fayzabad',' Provincia de Badajshan',' Afganistán']
	IATA_DESTINOS ['FCO'] = ['Aeropuerto de Roma-Fiumicino','Roma',' Lacio',' Italia','AIR_197617']
	IATA_DESTINOS ['FDO'] = ['Aeropuerto Internacional de San Fernando','San Fernando',' Buenos Aires',' Argentina','CIT_8988']
	IATA_DESTINOS ['FLR'] = ['Aeropuerto de Florencia','Florencia',' Toscana',' Italia','AIR_193818']
	IATA_DESTINOS ['FMA'] = ['Aeropuerto Internacional de Formosa','Ciudad de Formosa',' Formosa',' Argentina','AIR_193825']
	IATA_DESTINOS ['GHU'] = ['Aeropuerto de Gualeguaychú','Gualeguaychú',' Entre Ríos',' Argentina','CIT_2498']
	IATA_DESTINOS ['GPO'] = ['Aeropuerto de General Pico','General Pico',' La Pampa',' Argentina','CIT_2603']
	IATA_DESTINOS ['GRU'] = ['Aeropuerto Internacional de São Paulo-Guarulhos','São Paulo',' São Paulo',' Brasil','AIR_197727']
	IATA_DESTINOS ['HAJ'] = ['Aeropuerto de Hannover','Hannover',' Baja Sajonia',' Alemania','CIT_2737']
	IATA_DESTINOS ['HAM'] = ['Aeropuerto de Hamburgo','Hamburgo',' Hamburgo',' Alemania','AIR_194264']
	IATA_DESTINOS ['HEA'] = ['Aeropuerto de Herat','Herat',' Provincia de Herat',' Afganistán','AIR_194302']
	IATA_DESTINOS ['HEL'] = ['Aeropuerto de Helsinki-Vantaa','Helsinki',' Uusimaa',' Finlandia','CIT_2787']
	IATA_DESTINOS ['HHN'] = ['Aeropuerto de Fráncfort-Hahn','Fráncfort',' Renania del Norte-Westfalia',' Alemania','CIT_2313']
	IATA_DESTINOS ['HKJ'] = ['Aeropuerto Internacional de Hong Kong','Hong Kong',' Región Administrativa Especial de Hong Kong',' China','CIT_2838']
	IATA_DESTINOS ['HNL'] = ['Aeropuerto Internacional de Honolulu','Honolulu',' Hawái',' Estados Unidos','AIR_194401']
	IATA_DESTINOS ['ICN'] = ['Aeropuerto Internacional de Incheon','Incheon, Seúl',' Ciudad Metropolitana de Incheon',' Corea del Sur','AIR_194541']
	IATA_DESTINOS ['INV'] = ['Aeropuerto de Inverness','Inverness',' Escocia',' Reino Unido','CIT_3147']
#	IATA_DESTINOS ['IRJ'] = ['Aeropuerto Capitán Vicente Almandos Almonacid','Ciudad de La Rioja',' La Rioja',' Argentina']
	IATA_DESTINOS ['IST'] = ['Aeropuerto Internacional Atatürk','Estambul',' Provincia de Estambul',' Turquía','CIT_3209']
	IATA_DESTINOS ['IVL'] = ['Aeropuerto de Ivalo','Ivalo',' Laponia finlandesa',' Finlandia','AIR_194720']
#	IATA_DESTINOS ['JAA'] = ['Aeropuerto de Jalalabad','Jalalabad',' Provincia de Nangar',' Afganistán']
#	IATA_DESTINOS ['JER'] = ['Aeropuerto de Jersey','Saint Helier',' Jersey',' Reino Unido']
	IATA_DESTINOS ['JFK'] = ['Aeropuerto Internacional John F. Kennedy','Nueva York',' Nueva York',' Estados Unidos','AIR_196656']
	IATA_DESTINOS ['JSR'] = ['Aeropuerto de Jessore','Jessore',' Jessore',' Bangladés','AIR_194889']
	IATA_DESTINOS ['KBL'] = ['Aeropuerto Internacional de Kabul','Kabul',' Provincia de Kabul',' Afganistán','AIR_194953']
	IATA_DESTINOS ['KDH'] = ['Aeropuerto Internacional de Kandahar','Kandahar',' Provincia de Kandahar',' Afganistán','AIR_194990']
	IATA_DESTINOS ['KEF'] = ['Aeropuerto Internacional de Keflavík','Reikiavik',' Suðurnes',' Islandia','CIT_6327']
	IATA_DESTINOS ['LAX'] = ['Aeropuerto Internacional de Los Ángeles','Los Ángeles',' California',' Estados Unidos','AIR_195406']
	IATA_DESTINOS ['LGG'] = ['Aeropuerto de Lieja','Lieja',' Región Valona',' Bélgica','AIR_195498']
	IATA_DESTINOS ['LGS'] = ['Aeropuerto Internacional Comodoro Ricardo Salomón','Malargüe',' Mendoza',' Argentina','CIT_33547']
#	IATA_DESTINOS ['LEU'] = ['Aeropuerto de Andorra-La Seu','La Seu dUrgell, Andorra la Vella',' Cataluña',' España']
	IATA_DESTINOS ['LIM'] = ['Aeropuerto Internacional Jorge Chávez','Lima, Callao e Ica',' Lima',' Perú','AIR_195538']
	IATA_DESTINOS ['LTN'] = ['Aeropuerto de Londres-Luton','Londres',' Inglaterra',' Reino Unido','CIT_4188']
	IATA_DESTINOS ['LUQ'] = ['Aeropuerto Brigadier Mayor César Raúl Ojeda','Ciudad de San Luis',' San Luis',' Argentina','AIR_195914']
	IATA_DESTINOS ['MAD'] = ['Aeropuerto Adolfo Suárez Madrid-Barajas','Madrid',' Comunidad de Madrid',' España','AIR_195799']
	IATA_DESTINOS ['MCS'] = ['Aeropuerto de Monte Caseros','Monte Caseros',' Corrientes',' Argentina','CIT_4423']
	IATA_DESTINOS ['MDQ'] = ['Aeropuerto Internacional Astor Piazzolla','Mar del Plata',' Buenos Aires',' Argentina','AIR_195884']
#	IATA_DESTINOS ['MDX'] = ['Aeropuerto del Iberá','Mercedes',' Corrientes',' Argentina']
	IATA_DESTINOS ['MDZ'] = ['Aeropuerto Internacional Gobernador Francisco Gabrielli','Ciudad de Mendoza',' Mendoza',' Argentina','AIR_195890']
	IATA_DESTINOS ['MFM'] = ['Aeropuerto Internacional de Macao','Ciudad de Macao',' Macao','Vietnam','AIR_197873']
	IATA_DESTINOS ['MKC'] = ['Aeropuerto Urbano Charles B. Wheeler','Kansas City',' Misuri',' Estados Unidos','CIT_4414']
	IATA_DESTINOS ['MLH'] = ['Aeropuerto de Basilea-Mulhouse-Friburgo','Basilea, Mulhouse, Friburgo',' Alsacia','Suiza','CIT_936']
#	IATA_DESTINOS ['MPN'] = ['Base Aérea de Monte Agradable','Isla Soledad',' Islas Malvinas',' Reino Unido']
	IATA_DESTINOS ['MUC'] = ['Aeropuerto Internacional de Múnich-Franz Josef Strauss','Munich',' Baviera',' Alemania','CIT_4823']
	IATA_DESTINOS ['MZR'] = ['Aeropuerto de Mazar-e Sarif','Mazar-e Sarif',' Provincia de Balj',' Afganistán','AIR_196394']
#	IATA_DESTINOS ['NLK'] = ['Aeropuerto de la Isla Norfolk (en)','Kingston',' Norfolk',' Australia']
#	IATA_DESTINOS ['ORA'] = ['Aero Club Orán','San Ramón de la Nueva Orán',' Salta',' Argentina']
	IATA_DESTINOS ['ORK'] = ['Aeropuerto de Cork','Cork',' Múnster',' Irlanda','AIR_196834']
	IATA_DESTINOS ['ORY'] = ['Aeropuerto de París-Orly','París',' Isla de Francia',' Francia','CIT_5543']
	IATA_DESTINOS ['OYA'] = ['Aeropuerto Dr. Diego Nicolás Díaz Colodrero','Goya',' Corrientes',' Argentina','CIT_5504']
	IATA_DESTINOS ['PRA'] = ['Aeropuerto General Justo José de Urquiza','Paraná',' Entre Ríos',' Argentina','CIT_5877']
	IATA_DESTINOS ['PRQ'] = ['Aeropuerto de Presidencia Roque Sáenz Peña','Presidencia Roque Sáenz Peña',' Chaco',' Argentina','CIT_5892']
#	IATA_DESTINOS ['PSY'] = ['Aeropuerto de Puerto Argentino/Stanley','Puerto Argentino/Stanley',' Islas Malvinas',' Reino Unido']
#	IATA_DESTINOS ['QGY'] = ['Aeropuerto Internacional de Györ-Pér','Györ',' Győr-Moson-Sopron',' Hungría']
	IATA_DESTINOS ['QSA'] = ['Aeropuerto de Sabadell','Sabadell',' Cataluña',' España','CIT_6187']
	IATA_DESTINOS ['RAK'] = ['Aeropuerto de Marrakech-Menara','Marrakech',' Marrakech-Tensift-Al Hauz',' Marruecos','AIR_197433']
	IATA_DESTINOS ['RES'] = ['Aeropuerto Internacional de Resistencia','Ciudad de Resistencia',' Chaco',' Argentina','CIT_6333']
	IATA_DESTINOS ['REU'] = ['Aeropuerto de Reus','Reus -Tarragona',' Cataluña',' España','AIR_197509']
	IATA_DESTINOS ['RGA'] = ['Aeropuerto Internacional Gob. Ramón Trejo Noel','Río Grande',' Tierra del Fuego',' Argentina','CIT_6348']
	IATA_DESTINOS ['RHD'] = ['Aeropuerto Internacional Termas de Río Hondo','Termas de Río Hondo',' Santiago del Estero',' Argentina','AIR_197531']
	IATA_DESTINOS ['RLO'] = ['Aeropuerto Internacional Valle del Conlara','Merlo',' San Luis',' Argentina','AIR_197579']
	IATA_DESTINOS ['RRG'] = ['Aeropuerto Sir Gaëtan Duval','Rodrigues',' Rodrigues',' Mauricio','AIR_197638']
	#IATA_DESTINOS ['RSA'] = ['Aeropuerto de Santa Rosa','Ciudad de Santa Rosa',' La Pampa',' Argentina']
	IATA_DESTINOS ['SBZ'] = ['Aeropuerto Internacional de Sibiu','Sibiu',' Distrito de Sibiu',' Rumania','AIR_197764']
	IATA_DESTINOS ['SDE'] = ['Aeropuerto Vicecomodoro Ángel de la Paz Aragonés','Ciudad de Santiago del Estero',' Santiago del Estero',' Argentina','AIR_195914']
	IATA_DESTINOS ['SIN'] = ['Aeropuerto Internacional de Singapur-Changi','Singapur',' Consejo del Sudeste',' Singapur','CIT_6777']
	#IATA_DESTINOS ['SKG'] = ['Aeropuerto Internacional Macedonia','Tesalónica',' Macedonia Central',' Grecia']
	#IATA_DESTINOS ['SLA'] = ['Aeropuerto Internacional de Salta Martín Miguel de Güemes','Ciudad de Salta',' Salta',' Argentina']
	IATA_DESTINOS ['SNU'] = ['Aeropuerto Internacional Abel Santamaría','Santa Clara',' Villa Clara',' Cuba','CIT_6920']
	IATA_DESTINOS ['SXB'] = ['Aeropuerto de Estrasburgo','Estrasburgo',' Alsacia',' Francia','AIR_198275']
	IATA_DESTINOS ['SYD'] = ['Aeropuerto Internacional Kingsford Smith','Sídney',' Nueva Gales del Sur',' Australia','AIR_198299']
	IATA_DESTINOS ['THF'] = ['Aeropuerto de Berlín-Tempelhof','Berlín',' Berlín',' Alemania','CIT_627']
	IATA_DESTINOS ['TLV'] = ['Aeropuerto Internacional Ben Gurión','Tel Aviv',' Distrito Central',' Israel','CIT_7423']
	IATA_DESTINOS ['TPE'] = ['Aeropuerto Internacional de Taiwán Taoyuan','Taipéi',' Taoyuan',' Taiwán','AIR_198619']
	IATA_DESTINOS ['TTG'] = ['Aeropuerto de General Enrique Mosconi / Tartagal','Tartagal',' Salta',' Argentina','CIT_7563']
	IATA_DESTINOS ['UIO'] = ['Aeropuerto Internacional Mariscal Sucre','Quito',' Pichincha',' Ecuador','CIT_7697']
	IATA_DESTINOS ['USH'] = ['Aeropuerto Internacional Malvinas Argentinas','Ushuaia',' Tierra del Fuego',' Argentina','AIR_198892']
	IATA_DESTINOS ['UZU'] = ['Aeropuerto de Curuzú Cuatiá','Curuzú Cuatiá',' Corrientes',' Argentina','CIT_7824']
	IATA_DESTINOS ['VIE'] = ['Aeropuerto de Viena-Schwechat','Viena',' Viena',' Austria','AIR_198991']
	IATA_DESTINOS ['VLC'] = ['Aeropuerto de Valencia','Manises',' Comunidad Valenciana',' España','CIT_34228']
	IATA_DESTINOS ['VME'] = ['Aeropuerto de Villa Reynolds','Villa Mercedes',' San Luis',' Argentina','AIR_199026']
	IATA_DESTINOS ['WLG'] = ['Aeropuerto Internacional de Wellington','Wellington',' Wellington',' Nueva Zelanda','CIT_8117']
	IATA_DESTINOS ['XRY'] = ['Aeropuerto de Jerez','Jerez','Andalucía',' España','CIT_8327']
	IATA_DESTINOS ['YHZ'] = ['Aeropuerto Internacional de Halifax-Stanfield','Halifax',' Nueva Escocia',' Canadá','CIT_8626']
	IATA_DESTINOS ['YRB'] = ['Aeropuerto de Resolute Bay','Resolute',' Nunavut',' Canadá','AIR_199572']
	IATA_DESTINOS ['YUL'] = ['Aeropuerto Internacional Pierre Elliott Trudeau','Montreal',' Quebec',' Canadá','CIT_8549']
	IATA_DESTINOS ['YVR'] = ['Aeropuerto Internacional de Vancouver','Vancouver',' Columbia Británica',' Canadá','CIT_8708']
	IATA_DESTINOS ['YYZ'] = ['Aeropuerto Internacional Toronto Pearson','Toronto',' Ontario',' Canadá','CIT_8762']
	IATA_DESTINOS ['ZAG'] = ['Aeropuerto de Zagreb-Pleso','Zagreb',' Condado de Zagreb',' Croacia','CIT_8785','A199714']
	IATA_DESTINOS ['ZAZ'] = ['Aeropuerto de Zaragoza', 'Zaragoza','Flag of Aragon.svg Aragón', 'España','AIR_1997']
	
	#def start_requests(self):
		#Se rescata el codigo identificador de cada ciudad.
		#for iata_destino in self.IATA_DESTINOS:
		#	yield Request(url='https://www.despegar.cl/autocomplete/search?flow_code=FLIGHT_HOTEL&product=packages&locale=es_CL&query='+str(self.IATA_DESTINOS[iata_destino][1])
		#				 ,callback=self.parse_vuelo
		#				 ,dont_filter = True)
		#print(self.IATA_DESTINOS[iata][2])

	def start_requests (self):
		#if len(response.body)>100: 			#Condición para saber cuando entrega el mensaje de error
		#cadena_json =json.loads(response.body)
		#city_destino = cadena_json['data']['CITY'][0]['gid']
		hoy =  date.today()
		fecha_ida = hoy + timedelta(days=30)
		fecha_vuelta = hoy + timedelta(days=37)
		for iata_destino in self.IATA_DESTINOS:
			print(self.IATA_DESTINOS[iata_destino][1])
			for iata_origen in  self.IATA_CHILE:
				yield Request(url='https://www.despegar.cl/shapi/packages?page=1&pageSize=1000&back='+str(fecha_vuelta)+'&departure='+str(fecha_ida)+'&from='+str(self.IATA_CHILE[iata_origen][4])+'&to='+str(self.IATA_DESTINOS[iata_destino][4])+'&distribution=2&locale=es-CL'
						 ,callback=self.parse_data
						 ,dont_filter=True
						 ,meta = {'pais_origen':self.IATA_CHILE[iata_origen][1]
						 		 ,'pais_destino':self.IATA_DESTINOS[iata_destino][3]
						 		 ,'ciudad_origen':self.IATA_CHILE[iata_origen][3]
						 		 ,'ciudad_destino':self.IATA_DESTINOS[iata_destino][1]
						 		 })
	
			

	def parse_data (self,response):
		#Se obtienen fechas para obtener los pasajes del proximo mes
		cadena_json = json.loads(response.body.decode("utf-8"))
		#b = str(response.body).encode('utf-8')
		#cadena_json =json.loads(response.body)
		#fecha_ida = url_query_parameter(response.url, 'departure')
		#fecha_vuelta = url_query_parameter(response.url, 'back')
		if len(response.body)>1000:
		#cadena_json =json.loads(b'response.body)
		#Se genera extracción
			for ind in range(0,len(cadena_json['items'])):
				loader = DespegarLoader(response=response)
				for items in cadena_json['items'][ind]['items']:
					if items['type'].find('HOTEL')!=-1:
						loader.add_value('id_hotel'     				, items['id'])
						loader.add_value('nombre_hotel'  				, items['summary']['name'])
						loader.add_value('stars_hotel'   				, items['summary']['stars'])
						loader.add_value('fecha_checkin'				, items['summary']['checkin']['date'])
						loader.add_value('fecha_checkout'				, items['summary']['checkout']['date'])
						loader.add_value('hora_checkin'					, items['summary']['checkin']['time'])
						loader.add_value('hora_checkout'				, items['summary']['checkout']['time'])
						loader.add_value('ciudad_hotel'  				, items['summary']['city']['name'])
						loader.add_value('iata'		   					, items['summary']['city']['iata'])
						loader.add_value('latitude_hotel'	   			, str(items['summary']['location']['latitude'])[:8])
						loader.add_value('longitude_hotel'	   			, str(items['summary']['location']['longitude'])[:8])
						loader.add_value('direccion_hotel'     			, items['summary']['location']['address'])
						loader.add_value('rooms'		   				, items['summary']['rooms'][0]['name'])
						loader.add_value('noches'		   				, items['summary']['nights']['text'])
						loader.add_value('num_noches'	   				, items['summary']['nights']['value'])
						loader.add_value('rooms_pack'    				, items['summary']['distribution'])
						#loader.add_value('disponibilidad']				, items['availability']['text'])
						#loader.add_value('num_disponibilidad']	, items['availability']['available'])

					if items['type'].find('FLIGHT')!=-1:
						loader.add_value('id_vuelo'      				, items['id'])
						#DATOS IDA
						loader.add_value('duracion_vuelo_ida'			, items['summary']['outbound']['duration'])
						loader.add_value('escalas_ida'					, items['summary']['outbound']['stopovers']['text'])
						loader.add_value('num_escalas_id'				, items['summary']['outbound']['stopovers']['amount'])
						#DATOS VUELTA
						loader.add_value('duracion_vuelo_vuelta'		, items['summary']['inbound']['duration'])
						loader.add_value('escalas_vuelta'				, items['summary']['inbound']['stopovers']['text'])
						loader.add_value('num_escalas_vuelta'			, items['summary']['inbound']['stopovers']['amount'])

					if items['type'].find('TRAVEL_ASSISTANCE')!=-1:
						loader.add_value('id_asistencia_viajes'  		   , items['id'])
						loader.add_value('descripcion_asistencia_viajes'   , items['description'])
						loader.add_value('anuncio_asistencia_viajes'	   , items['advertisement'])
					
				loader.add_value('codigo_moneda_clp'				,str(cadena_json['items'][ind]['prices']['CLP']['currency']['code']))
				loader.add_value('precio_anunciado_total_clp'      	,str(cadena_json['items'][ind]['prices']['CLP']['advertised']['total']))
				loader.add_value('precio_anunciado_bruto_clp'       ,str(cadena_json['items'][ind]['prices']['CLP']['advertised']['gross']))
				loader.add_value('precio_anunciado_por_clp'        	,str(cadena_json['items'][ind]['prices']['CLP']['advertised']['text']))
				loader.add_value('anunciado_cargos'             	,str(cadena_json['items'][ind]['prices']['CLP']['advertised']['charges']))
				loader.add_value('descuento_clp'                  	,str(cadena_json['items'][ind]['prices']['CLP']['discount']['value']))
				loader.add_value('total_clp'                     	,str(cadena_json['items'][ind]['prices']['CLP']['total']['value']))
				loader.add_value('total_cargos_clp'                 ,str(cadena_json['items'][ind]['prices']['CLP']['total']['charges']))
				#Se puede necesitar iteracion for
				loader.add_value('breakdown_base_clp'              	,str(cadena_json['items'][ind]['prices']['CLP']['breakdown'][0]['value']))
				loader.add_value('breakdown_base_por_clp'           ,str(cadena_json['items'][ind]['prices']['CLP']['breakdown'][0]['text']))
				loader.add_value('breakdown_cargos_clp'            	,str(cadena_json['items'][ind]['prices']['CLP']['breakdown'][1]['value']))
				loader.add_value('breakdown_cargos_por'         	,str(cadena_json['items'][ind]['prices']['CLP']['breakdown'][1]['text']))

				#DATOS DE VALORES POR USD

				loader.add_value('codigo_moneda_usd'              	,str(cadena_json['items'][ind]['prices']['USD']['currency']['code']))
				loader.add_value('precio_anunciado_total_usd'       ,str(cadena_json['items'][ind]['prices']['USD']['advertised']['total']))
				loader.add_value('precio_anunciado_bruto_usd'       ,str(cadena_json['items'][ind]['prices']['USD']['advertised']['gross']))
				loader.add_value('precio_anunciado_por_usd'        	,str(cadena_json['items'][ind]['prices']['USD']['advertised']['text']))
				loader.add_value('anunciado_cargos_usd'          	,str(cadena_json['items'][ind]['prices']['USD']['advertised']['charges']))
				loader.add_value('descuento_usd'              	    ,str(cadena_json['items'][ind]['prices']['USD']['discount']['value']))
				loader.add_value('total_usd'                     	,str(cadena_json['items'][ind]['prices']['USD']['total']['value']))
				loader.add_value('total_cargos_usd'                 ,str(cadena_json['items'][ind]['prices']['USD']['total']['charges']))
				#Se puede necesitar iteracion for
				loader.add_value('breakdown_base_usd'               ,str(cadena_json['items'][ind]['prices']['USD']['breakdown'][0]['value']))
				loader.add_value('breakdown_base_por_usd'           ,str(cadena_json['items'][ind]['prices']['USD']['breakdown'][0]['text']))
				loader.add_value('breakdown_cargos_usd'             ,str(cadena_json['items'][ind]['prices']['USD']['breakdown'][1]['value']))
				loader.add_value('breakdown_cargos_por_usd'         ,str(cadena_json['items'][ind]['prices']['USD']['breakdown'][1]['text']))

				#origen y destino
				loader.add_value('ciudad_destino'					, response.meta['ciudad_destino'])
				loader.add_value('pais_destino'						, response.meta['pais_destino'])
				loader.add_value('ciudad_origen'					, response.meta['ciudad_origen'])
				loader.add_value('pais_origen'						, response.meta['pais_origen'])

				loader.add_value('iata_destino'						, cadena_json['destination']['iata'])
				loader.add_value('gid_destino'						, cadena_json['destination']['gid'])
				loader.add_value('nombre_destino'					, cadena_json['destination']['name'])

				loader.add_value('latitude_destino'					, str(cadena_json['destination']['coordinate']['latitude'])[:8])
				loader.add_value('longitude_destino'				, str(cadena_json['destination']['coordinate']['longitude'])[:8])

				#origen
				loader.add_value('iata_origen' 						, cadena_json['origin']['iata'])
				loader.add_value('gid_origen'						, cadena_json['origin']['gid'])
				loader.add_value('nombre_origen'					, cadena_json['origin']['name'])

				#aerolines
				for aerolinea in  cadena_json['airlines']:
					loader.add_value( 'id_aerolinea'				, cadena_json['airlines'][aerolinea]['name'])
					loader.add_value( 'nombre_aerolinea'			, cadena_json['airlines'][aerolinea]['name'])
					loader.add_value( 'score_general_aerolinea'		, cadena_json['airlines'][aerolinea]['rating']['general']['text'])
					loader.add_value( 'points_aerolinea'			, cadena_json['airlines'][aerolinea]['rating']['score']['points'])
				loader.add_value( 'fuente'							, "www.despegar.cl")
				loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
				loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

				yield(loader.load_item())
