# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request,FormRequest
from turismo.items import CochaLoader
from datetime import datetime, date, time, timedelta
import time
import json

class cochaSpider(CrawlSpider):
	name = 'cocha_com'
	#allowed_domains = ['despegar.cl']
	#start_urls= ['https://www.despegar.cl/shapi/packages?page=1&pageSize=1000&back=2017-09-16&departure=2017-09-04&from=CIT_6624&to=CIT_4088&distribution=2&locale=es-CL']
	#start_urls= ['https://www.despegar.cl/shapi/packages?page=1&pageSize=1000&back=2017-09-16&departure=2017-09-04&from=CIT_6624&to=CIT_5543&distribution=2&locale=es-CL']

	IATA_CHILE ={}
	IATA_DESTINOS = {}		

	IATA_CHILE ['ARI'] = ['Aeropuerto Internacional Chacalluta','Arica',' Arica y Parinacota',' Chile','CIT_349']
	IATA_CHILE ['IPC'] = ['Aeropuerto Internacional Mataveri','Isla de Pascua',' Valparaíso',' Chile','CIT_3164']
	IATA_CHILE ['PUQ'] = ['Aeropuerto Internacional Presidente Carlos Ibáñez del Campo','Punta Arenas',' Magallanes y Antártica Chilena',' Chile','CIT_5970']
	IATA_CHILE ['SCL'] = ['Aeropuerto Internacional Comodoro Arturo Merino Benítez','Santiago de Chile',' Metropolitana de Santiago',' Chile','CIT_6624']
	IATA_CHILE ['ZCO'] = ['Aeropuerto Internacional La Araucanía','Freire (Temuco)','Chile','La Araucanía','AIR_827764']
	IATA_CHILE ['IQQ'] = ['Aeropuerto Internacional Diego Aracena','Iquique','Tarapacá','Chile','AIR_194666']
	IATA_CHILE ['PMC'] = ['Aeropuerto Internacional El Tepual','Puerto Montt','Los Lagos','Chile','AIR_197162']


	IATA_DESTINOS ['MVD'] =	['Aeropuerto Internacional Carrasco', 'Montevideo','Montevideo','Uruguay','CIT_4844']
	IATA_DESTINOS ['HAV'] =	['Aeropuerto Internacional Jose Marti','La Habana','La Habana','Cuba','AIR_1344841']
	IATA_DESTINOS ['CUN'] =	['Aeropuerto Internacional Cancún','Cancún','Cancún','México','AIR_193176']
	IATA_DESTINOS ['PDP'] =	['Aeropuerto Internacional Capitan Corbeta CA Curbelo','Punta del Este','Punta del Este','Uruguay','AIR_197009']
	IATA_DESTINOS ['AFA'] = ['Aeropuerto Internacional Suboficial Ayudante Santiago Germano','San Rafael',' Mendoza',' Argentina','CIT_102']
	IATA_DESTINOS ['AOL'] = ['Aeropuerto Internacional de Paso de los Libres','Paso de los Libres',' Córdoba',' Argentina','CIT_299']
	#IATA_DESTINOS ['ASU'] = ['Aeropuerto Internacional Silvio Pettirossi','Luque',' Central',' Paraguay']
	IATA_DESTINOS ['ATH'] = ['Aeropuerto Internacional Eleftherios Venizelos','Atenas',' Ática',' Grecia','AIR_192061']
	IATA_DESTINOS ['AUA'] = ['Aeropuerto Internacional Reina Beatrix','Oranjestad',' Aruba',' Países Bajos','CIT_420']
	IATA_DESTINOS ['BOG'] = ['Aeropuerto Internacional El Dorado','Bogotá',' Bogotá',' Colombia','AIR_192510']
	IATA_DESTINOS ['CGN'] = ['Aeropuerto Internacional de Colonia/Bonn Konrad Adenauer','Colonia, Bonn',' Renania del Norte-Westfalia',' Alemania','CIT_1250']
	#IATA_DESTINOS ['CNQ'] = ['Aeropuerto Internacional Doctor Fernándo Piragine Niveyro','Ciudad de Corrientes',' Corrientes',' Argentina']
	IATA_DESTINOS ['CCS'] = ['Aeropuerto Internacional de Maiquetía Simón Bolívar','Caracas',' Vargas',' Venezuela','CIT_1165']
	IATA_DESTINOS ['DAY'] = ['Aeropuerto Internacional James M. Cox-Dayton','Dayton',' Ohio',' Estados Unidos','CIT_1674']#REVISAR EJEMPLOS
	#IATA_DESTINOS ['DJE'] = ['Aeropuerto Internacional de Yerba-Zarzis','Yerba',' Medenine',' Túnez']
	IATA_DESTINOS ['DME'] = ['Aeropuerto Internacional de Moscú-Domodédovo','Moscú',' Óblast de Moscú',' Rusia','CIT_4700']
	IATA_DESTINOS ['DXB'] = ['Aeropuerto Internacional de Dubái','Dubái',' Dubái',' Emiratos Árabes Unidos','CIT_1899']
	IATA_DESTINOS ['EZE'] = ['Aeropuerto Internacional Ministro Pistarini','Ezeiza',' Buenos Aires',' Argentina','CIT_34089']
	IATA_DESTINOS ['FDO'] = ['Aeropuerto Internacional de San Fernando','San Fernando',' Buenos Aires',' Argentina','CIT_8988']
	IATA_DESTINOS ['FMA'] = ['Aeropuerto Internacional de Formosa','Ciudad de Formosa',' Formosa',' Argentina','AIR_193825']
	IATA_DESTINOS ['GRU'] = ['Aeropuerto Internacional de São Paulo-Guarulhos','São Paulo',' São Paulo',' Brasil','AIR_197727']
	IATA_DESTINOS ['HKJ'] = ['Aeropuerto Internacional de Hong Kong','Hong Kong',' Región Administrativa Especial de Hong Kong',' China','CIT_2838']
	IATA_DESTINOS ['HNL'] = ['Aeropuerto Internacional de Honolulu','Honolulu',' Hawái',' Estados Unidos','AIR_194401']
	IATA_DESTINOS ['ICN'] = ['Aeropuerto Internacional de Incheon','Incheon, Seúl',' Ciudad Metropolitana de Incheon',' Corea del Sur','AIR_194541']
	IATA_DESTINOS ['IST'] = ['Aeropuerto Internacional Atatürk','Estambul',' Provincia de Estambul',' Turquía','CIT_3209']
	IATA_DESTINOS ['JFK'] = ['Aeropuerto Internacional John F. Kennedy','Nueva York',' Nueva York',' Estados Unidos','AIR_196656']
	IATA_DESTINOS ['KBL'] = ['Aeropuerto Internacional de Kabul','Kabul',' Provincia de Kabul',' Afganistán','AIR_194953']
	IATA_DESTINOS ['KDH'] = ['Aeropuerto Internacional de Kandahar','Kandahar',' Provincia de Kandahar',' Afganistán','AIR_194990']
	IATA_DESTINOS ['KEF'] = ['Aeropuerto Internacional de Keflavík','Reikiavik',' Suðurnes',' Islandia','CIT_6327']
	IATA_DESTINOS ['LAX'] = ['Aeropuerto Internacional de Los Ángeles','Los Ángeles',' California',' Estados Unidos','AIR_195406']
	IATA_DESTINOS ['LGS'] = ['Aeropuerto Internacional Comodoro Ricardo Salomón','Malargüe',' Mendoza',' Argentina','CIT_33547']
	IATA_DESTINOS ['LIM'] = ['Aeropuerto Internacional Jorge Chávez','Lima, Callao e Ica',' Lima',' Perú','AIR_195538']
	IATA_DESTINOS ['MDQ'] = ['Aeropuerto Internacional Astor Piazzolla','Mar del Plata',' Buenos Aires',' Argentina','AIR_195884']
	IATA_DESTINOS ['MDZ'] = ['Aeropuerto Internacional Gobernador Francisco Gabrielli','Ciudad de Mendoza',' Mendoza',' Argentina','AIR_195890']
	IATA_DESTINOS ['MFM'] = ['Aeropuerto Internacional de Macao','Ciudad de Macao',' Macao','Vietnam','AIR_197873']
	IATA_DESTINOS ['MUC'] = ['Aeropuerto Internacional de Múnich-Franz Josef Strauss','Munich',' Baviera',' Alemania','CIT_4823']
	#	IATA_DESTINOS ['QGY'] = ['Aeropuerto Internacional de Györ-Pér','Györ',' Győr-Moson-Sopron',' Hungría']
	IATA_DESTINOS ['RES'] = ['Aeropuerto Internacional de Resistencia','Ciudad de Resistencia',' Chaco',' Argentina','CIT_6333']
	IATA_DESTINOS ['RGA'] = ['Aeropuerto Internacional Gob. Ramón Trejo Noel','Río Grande',' Tierra del Fuego',' Argentina','CIT_6348']
	IATA_DESTINOS ['RHD'] = ['Aeropuerto Internacional Termas de Río Hondo','Termas de Río Hondo',' Santiago del Estero',' Argentina','AIR_197531']
	IATA_DESTINOS ['RLO'] = ['Aeropuerto Internacional Valle del Conlara','Merlo',' San Luis',' Argentina','AIR_197579']
	IATA_DESTINOS ['SBZ'] = ['Aeropuerto Internacional de Sibiu','Sibiu',' Distrito de Sibiu',' Rumania','AIR_197764']
	IATA_DESTINOS ['SIN'] = ['Aeropuerto Internacional de Singapur-Changi','Singapur',' Consejo del Sudeste',' Singapur','CIT_6777']
	#IATA_DESTINOS ['SKG'] = ['Aeropuerto Internacional Macedonia','Tesalónica',' Macedonia Central',' Grecia']
	#IATA_DESTINOS ['SLA'] = ['Aeropuerto Internacional de Salta Martín Miguel de Güemes','Ciudad de Salta',' Salta',' Argentina']
	IATA_DESTINOS ['SNU'] = ['Aeropuerto Internacional Abel Santamaría','Santa Clara',' Villa Clara',' Cuba','CIT_6920']
	IATA_DESTINOS ['SYD'] = ['Aeropuerto Internacional Kingsford Smith','Sídney',' Nueva Gales del Sur',' Australia','AIR_198299']
	IATA_DESTINOS ['TLV'] = ['Aeropuerto Internacional Ben Gurión','Tel Aviv',' Distrito Central',' Israel','CIT_7423']
	IATA_DESTINOS ['TPE'] = ['Aeropuerto Internacional de Taiwán Taoyuan','Taipéi',' Taoyuan',' Taiwán','AIR_198619']
	IATA_DESTINOS ['UIO'] = ['Aeropuerto Internacional Mariscal Sucre','Quito',' Pichincha',' Ecuador','CIT_7697']
	IATA_DESTINOS ['USH'] = ['Aeropuerto Internacional Malvinas Argentinas','Ushuaia',' Tierra del Fuego',' Argentina','AIR_198892']
	IATA_DESTINOS ['WLG'] = ['Aeropuerto Internacional de Wellington','Wellington',' Wellington',' Nueva Zelanda','CIT_8117']
	IATA_DESTINOS ['YHZ'] = ['Aeropuerto Internacional de Halifax-Stanfield','Halifax',' Nueva Escocia',' Canadá','CIT_8626']
	IATA_DESTINOS ['YUL'] = ['Aeropuerto Internacional Pierre Elliott Trudeau','Montreal',' Quebec',' Canadá','CIT_8549']
	IATA_DESTINOS ['YVR'] = ['Aeropuerto Internacional de Vancouver','Vancouver',' Columbia Británica',' Canadá','CIT_8708']
	IATA_DESTINOS ['YYZ'] = ['Aeropuerto Internacional Toronto Pearson','Toronto',' Ontario',' Canadá','CIT_8762']


	#Nuevos
	#Argentina
	IATA_DESTINOS['BRC'] = ['Aeropuerto Internacional Teniente Luis Candelaria','Bariloche','Río Negro','Argentina']
	IATA_DESTINOS['NQN'] = ['Aeropuerto Internacional Presidente Perón','Neuquén','Neuquén','Argentina']
	IATA_DESTINOS['AOL'] = ['Aeropuerto Internacional de Paso de los Libres','Paso de los Libres','Corrientes','Argentina']
	#Peru
	IATA_DESTINOS['TCQ'] = ['Aeropuerto Internacional Coronel FAP Carlos Ciriani Santa Rosa','Tacna','Tacna','Perú']
	#Brasil
	IATA_DESTINOS['CWB'] = ['Aeropuerto Internacional Augusto Severo','Curitiba','Paraná','Brasil']
	IATA_DESTINOS['MCP'] = ['Aeropuerto Internacional de Macapá','Macapá','Amapá','Brasil']
	IATA_DESTINOS['SLZ'] = ['Aeropuerto Internacional Mariscal Cunha Machado','Sāo Luiz','Maranhāo','Brasil']
	IATA_DESTINOS['JPA'] = ['Aeropuerto Internacional Presidente Castro Pinto','João Pessoa','Paraíba','Brasil']
	IATA_DESTINOS['GIG'] = ['Aeropuerto Internacional de Galeão','Río de Janeiro','Río de Janeiro','Brasil']
	IATA_DESTINOS['BVB'] = ['Aeropuerto Internacional Boa Vista','Boa Vista','Roraima','Brasil']
	IATA_DESTINOS['SSA'] = ['Aeropuerto Internacional Deputado Luís Eduardo Magalhães','Salvador','Bahía','Brasil']
	IATA_DESTINOS['BPS'] = ['Aeropuerto Internacional de Porto Seguro','Porto Seguro','Bahía','Brasil']
	IATA_DESTINOS['MAO'] = ['Aeropuerto Internacional Eduardo Gomes','Manaus','Amazonas','Brasil']
	IATA_DESTINOS['TBT'] = ['Aeropuerto Internacional de Tabatinga','Tabatinga','Amazonas','Brasil']
	IATA_DESTINOS['CGB'] = ['Aeropuerto Internacional Marechal Rondon','Cuiabá','Mato Grosso','Brasil']
	IATA_DESTINOS['CGR'] = ['Aeropuerto Internacional Campo Grande','Campo Grande','Mato Grosso do Sul','Brasil']
	IATA_DESTINOS['CMG'] = ['Aeropuerto Internacional Corumba','Corumbá','Mato Grosso do Sul','Brasil']
	IATA_DESTINOS['CZS'] = ['Aeropuerto Internacional Cruzeiro do Sul','Cruzeiro do Sul','Acre','Brasil']
	IATA_DESTINOS['FOR'] = ['Aeropuerto Internacional Pinto Martins','Fortaleza','Ceará','Brasil']
	IATA_DESTINOS['IGU'] = ['Aeropuerto Internacional Foz do Iguaçu','Foz do Iguaçu','Paraná','Brasil']
	IATA_DESTINOS['REC'] = ['Aeropuerto Internacional Gilberto Freyre','Recife','Pernambuco','Brasil']
	IATA_DESTINOS['GRU'] = ['Aeropuerto Internacional de Guarulhos Governador André Franco Montoro','São Paulo','São Paulo','Brasil']
	IATA_DESTINOS['PMG'] = ['Aeropuerto Internacional Ponta Pora','Ponta Porã','Mato Grosso do Sul','Brasil']
	IATA_DESTINOS['BSB'] = ['Aeropuerto Internacional Presidente Juscelino Kubitschek','Brasilia','Distrito Federal','Brasil']
	IATA_DESTINOS['FLN'] = ['Aeropuerto Internacional Hercílio Luz','Florianópolis','Santa Catarina','Brasil']
	IATA_DESTINOS['PVH'] = ['Aeropuerto Internacional Governador Jorge Teixeira','Porto Velho','Rondônia','Brasil']
	IATA_DESTINOS['BEL'] = ['Aeropuerto Internacional de Belém','Belém','Pará','Brasil']
	IATA_DESTINOS['JOI'] = ['Aeropuerto Internacional Lauro Carneiro de Loyola','Joinville','Santa Catarina','Brasil']
	IATA_DESTINOS['BGX'] = ['Aeropuerto Internacional Comandante Gustavo Kraemer','Bagé','Rio Grande do Sul','Brasil']
	IATA_DESTINOS['POA'] = ['Aeropuerto Internacional Salgado Filho','Porto Alegre','Rio Grande do Sul','Brasil']
	IATA_DESTINOS['CNF'] = ['Aeropuerto Internacional Tancredo Neves','Belo Horizonte','Minas Gerais','Brasil']
	IATA_DESTINOS['VCP'] = ['Aeropuerto Internacional Viracopos','Campinas','São Paulo','Brasil']
	#Colombia

	IATA_DESTINOS['BOG'] = ['Aeropuerto Internacional El Dorado','Bogotá','Distrito Capital','Colombia']
	IATA_DESTINOS['CLO'] = ['Aeropuerto Internacional Alfonso Bonilla Aragón','Cali','Valle del Cauca','Colombia']
	IATA_DESTINOS['CTG'] = ['Aeropuerto Internacional Rafael Núñez','Cartagena','Bolívar','Colombia']
	IATA_DESTINOS['MDE'] = ['Aeropuerto Internacional José María Córdova','Medellín','Antioquia','Colombia']
	IATA_DESTINOS['PEI'] = ['Aeropuerto Internacional Matecaña','Pereira','Risaralda','Colombia']
	IATA_DESTINOS['BAQ'] = ['Aeropuerto Internacional Ernesto Cortissoz','Barranquilla','Atlántico','Colombia']
	IATA_DESTINOS['SMR'] = ['Aeropuerto Internacional Simón Bolívar','Santa Marta','Magdalena','Colombia']
	IATA_DESTINOS['CUC'] = ['Aeropuerto Internacional Camilo Daza','Cúcuta','Norte de Santander','Colombia']
	IATA_DESTINOS['ADZ'] = ['Aeropuerto Internacional Gustavo Rojas Pinilla','San Andrés','San Andrés y Providencia','Colombia']
	IATA_DESTINOS['BGA'] = ['Aeropuerto Internacional Palonegro','Bucaramanga','Santander','Colombia']
	IATA_DESTINOS['RCH'] = ['Aeropuerto Internacional Almirante Padilla','Riohacha','La Guajira','Colombia']
	IATA_DESTINOS['LET'] = ['Aeropuerto Internacional Alfredo Vásquez Cobo','Leticia','Amazonas','Colombia']
	IATA_DESTINOS['AXM'] = ['Aeropuerto Internacional El Edén','Armenia','Quindío','Colombia']

	#usa

	IATA_DESTINOS['ATL'] = ['Aeropuerto Internacional Hartsfield-Jackson','Atlanta','GA','Estados Unidos']
	IATA_DESTINOS['LAX'] = ['Aeropuerto Internacional de Los Ángeles','Los Ángeles','CA','Estados Unidos']
	IATA_DESTINOS['ORD'] = ['Aeropuerto Internacional OHare','Chicago','IL','Estados Unidos']
	IATA_DESTINOS['DFW'] = ['Aeropuerto Internacional de Dallas-Fort Worth','Dallas/Fort Worth','TX','Estados Unidos']
	IATA_DESTINOS['JFK'] = ['Aeropuerto Internacional John F. Kennedy','Nueva York','NY','Estados Unidos']
	IATA_DESTINOS['DEN'] = ['Aeropuerto Internacional de Denver','Denver','CO','Estados Unidos']
	IATA_DESTINOS['SFO'] = ['Aeropuerto Internacional de San Francisco','San Francisco','CA','Estados Unidos']
	IATA_DESTINOS['CLT'] = ['Aeropuerto Internacional de Charlotte-Douglas','Charlotte','NC','Estados Unidos']
	IATA_DESTINOS['LAS'] = ['Aeropuerto Internacional McCarran','Las Vegas','NV','Estados Unidos']
	IATA_DESTINOS['PHX'] = ['Aeropuerto Internacional de Phoenix-Sky Harbor','Phoenix','AZ','Estados Unidos']
	IATA_DESTINOS['MIA'] = ['Aeropuerto Internacional de Miami','Miami','FL','Estados Unidos']
	IATA_DESTINOS['IAH'] = ['Aeropuerto Intercontinental George Bush','Houston','TX','Estados Unidos']
	IATA_DESTINOS['SEA'] = ['Aeropuerto Internacional de Seattle-Tacoma','Seattle','WA','Estados Unidos']
	IATA_DESTINOS['MCO'] = ['Aeropuerto Internacional de Orlando','Orlando','FL','Estados Unidos']
	IATA_DESTINOS['EWR'] = ['Aeropuerto Internacional Libertad de Newark','Newark/Nueva York','NJ','Estados Unidos']
	IATA_DESTINOS['MSP'] = ['Aeropuerto Internacional de Minneapolis-Saint Paul','Minneapolis/St. Paul','MN','Estados Unidos']
	IATA_DESTINOS['BOS'] = ['Aeropuerto Internacional Logan','Boston','MA','Estados Unidos']
	IATA_DESTINOS['DTW'] = ['Aeropuerto Internacional de Detroit','Detroit','MI','Estados Unidos']
	IATA_DESTINOS['PHL'] = ['Aeropuerto Internacional de Filadelfia','Filadelfia','PA','Estados Unidos']
	IATA_DESTINOS['LGA'] = ['Aeropuerto LaGuardia','Nueva York','NY','Estados Unidos']
	IATA_DESTINOS['FLL'] = ['Aeropuerto Internacional de Fort Lauderdale-Hollywood','Fort Lauderdale','FL','Estados Unidos']
	IATA_DESTINOS['BWI'] = ['Aeropuerto Internacional de Baltimore-Washington','Baltimore/Washington D. C.','MD','Estados Unidos']
	IATA_DESTINOS['IAD'] = ['Aeropuerto Internacional de Washington-Dulles','Washington, D. C.','VA','Estados Unidos']
	IATA_DESTINOS['MDW'] = ['Aeropuerto Internacional Midway','Chicago','IL','Estados Unidos']
	IATA_DESTINOS['SLC'] = ['Aeropuerto Internacional de Salt Lake City','Salt Lake City','UT','Estados Unidos']
	IATA_DESTINOS['DCA'] = ['Aeropuerto Nacional Ronald Reagan de Washington','Washington, D. C.','VA','Estados Unidos']
	IATA_DESTINOS['SAN'] = ['Aeropuerto Internacional de San Diego','San Diego','CA','Estados Unidos']
	IATA_DESTINOS['HNL'] = ['Aeropuerto Internacional de Honolulu','Honolulu','HI','Estados Unidos']
	IATA_DESTINOS['TPA'] = ['Aeropuerto Internacional de Tampa','Tampa','FL','Estados Unidos']
	IATA_DESTINOS['PDX'] = ['Aeropuerto Internacional de Portland','Portland','OR','Estados Unidos']

	def start_requests(self):
		for iata_origen in self.IATA_CHILE:
			for iata_destino in self.IATA_DESTINOS:
				yield Request( url = 'https://dynamic-pkg-mid.cocha.com/v1/autocomplete/'+iata_destino
							 , callback = self.parse_code
							 , dont_filter = True
							 , meta = {'iata_origen':iata_origen
							 		  ,'iata_destino':iata_destino
							 		  }
							)

	def parse_code(self,response):
		if len(str(response.body))>50:
			hoy =  date.today()
			fecha_ida = hoy + timedelta(days=30)
			fecha_vuelta = hoy + timedelta(days=37)
			#cadena_json =json.loads(response.body)
			cadena_json = json.loads(response.body.decode("utf-8"))
			datos_variables = response.meta['iata_origen']+'/'+response.meta['iata_destino']+'/'+str(cadena_json[0]['otherIds'][0]['ean'])+'/'+str(fecha_ida)+'/'+str(fecha_vuelta)
			yield Request(url='https://dynamic-pkg-mid.cocha.com/v1/search/'+datos_variables+'?airlines=&room1=2&stops=3&cabin=Y'
						 ,callback =self.parse_vuelo
						 ,dont_filter = True
						 ,meta = {'iata_origen':response.meta['iata_origen']
						 		 ,'iata_destino':response.meta['iata_destino']
						 		 ,'fecha_ida':str(fecha_ida)
								 ,'fecha_vuelta':str(fecha_vuelta)
						 		 }
						)

	def parse_vuelo(self,response):
		js = json.loads(response.body.decode("utf-8"))
		#js =json.loads(response.body)
		for ind in js['hotels']['found']:
			loader = CochaLoader(response=response)
			loader.add_value( 'nombre_hotel'			,	 ind['name'])
			loader.add_value( 'hotel_id'				,	 ind['hotelID'])
			#loader.add_value( 'direccion_origen'		,	 self.IATA_CHILE[response.meta['iata_origen']][1])
			loader.add_value( 'ciudad_origen'			,	 self.IATA_CHILE[response.meta['iata_origen']][1])
			loader.add_value( 'pais_origen'				,	 self.IATA_CHILE[response.meta['iata_origen']][3])	
			loader.add_value( 'codigo_pais_origen'		,	 'CHL')
			loader.add_value( 'codigo_aero_origen'		,	 response.meta['iata_origen'])
			loader.add_value( 'direccion_destino'		,	 ind['address1'])
			loader.add_value( 'ciudad_destino'			,	 ind['city'])
			loader.add_value( 'pais_destino'			,	 self.IATA_DESTINOS[response.meta['iata_destino']][3])	
			loader.add_value( 'codigo_pais_destino'		,	 ind['countryCode'])
			loader.add_value( 'codigo_aero_destino'		,	 ind['airportCode'])
			loader.add_value( 'tarifa_alta'				,	 ind['highRate'])
			loader.add_value( 'tarifa_baja'				,	 ind['lowRate']) 
			loader.add_value( 'codigo_tarifa'			,	 ind['rateCurrencyCode'])
			loader.add_value( 'latitud'					,	 str(ind['latitude'])[:8])
			loader.add_value( 'longitud'				,	 str(ind['longitude'])[:8])
			#Se entrega precio original
			loader.add_value( 'precio'					,	 str(ind['price']['just']['usd']['rate'])[:8])
			loader.add_value( 'descuento_moneda'		,	 str(int(ind['price']['just']['usd']['discount']))[:8])
			loader.add_value( 'precio_oferta_moneda'	,	 str(int(ind['price']['just']['usd']['rate'])+41)[:8])
			loader.add_value( 'precio_sin_oferta_moneda',	 str((int(ind['price']['just']['usd']['rate'])+41)+int(ind['price']['just']['usd']['discount']))[:8])

			loader.add_value( 'precio_oferta_clp'		,	 str(int(ind['price']['just']['usd']['rate'])+41*int(js['exchangeRate']))[:8])
			loader.add_value( 'precio_sin_oferta_clp'	,	 str(int(ind['price']['just']['usd']['rate'])+41+int(ind['price']['just']['usd']['discount'])*int(js['exchangeRate']))[:8])
			loader.add_value( 'hotel_rating'			,	 ind['hotelRating'])
			loader.add_value( 'hotel_rating_display'	,	 ind['hotelRatingDisplay'])
			loader.add_value( 'exchangeRate'			,	 js['exchangeRate'])
			loader.add_value( 'id_publicaciones'		,	 js['id'])
			#Variables de tiempo
			loader.add_value( 'fecha_checkin'			,	 response.meta['fecha_ida'])
			loader.add_value( 'fecha_checkout'			,	 response.meta['fecha_vuelta']) 
			loader.add_value( 'fuente'					,	 "www.cocha.com")
			loader.add_value( 'fecha_extraccion'		,	 time.strftime("%d/%m/%Y"))
			loader.add_value( 'hora_extraccion'			,	 time.strftime("%H:%M:%S"))

			yield (loader.load_item())
			





