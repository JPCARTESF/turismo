# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from turismo.items import ChileestuyoLoader
import time

class ChileesTuyo(CrawlSpider):
	name = 'chileestuyo_cl'
	#allowed_domains = ['clarochile.cl']
	start_urls = ["http://www.chileestuyo.cl/catalogo/buscar-paquetes/"
				 ,"http://www.chileestuyo.cl/catalogo/buscar-agencias/"
				 ,"http://www.chileestuyo.cl/catalogo/buscar-transporte/"
				 ,"http://www.chileestuyo.cl/catalogo/buscar-actividad/"
				 ,"http://www.chileestuyo.cl/catalogo/buscar-gastronomia/"
				 ,"http://www.chileestuyo.cl/catalogo/buscar-alojamiento/"
				 ]

	rules = (
			Rule(LinkExtractor(allow=('page\/\d+\/')),follow=True,callback='parse_page'),
	)


	def parse_page(self,response):
		for link in  response.xpath("//a[@class='btn ver-mas']/@href").extract():
			yield Request(url=link
						 ,callback=self.parse_data)
		#print(response.xpath("//p[@class='titulo']//text()"))
	def parse_data(self,response):
		loader = ChileestuyoLoader(response=response)
		loader.add_xpath( 'id_producto',"//*[@name='id_servicio']/@value")
		loader.add_xpath( 'nombre',"//*[@name='titulo']/@value")
		loader.add_xpath( 'categoria',"//div[@class='breadcrumb flat']//text()")
		loader.add_xpath( 'detalles_precio',"//div[@class='alojamiento-sidebar']//*[@class='set-price']//text()")
		loader.add_xpath( 'tipo_producto',"//div[@class='alojamiento-sidebar']//*[@class='desde-txt-dos']//text()")
		loader.add_xpath( 'telefono',"//div[@class='alojamiento-sidebar']/p[5]//text()")
		loader.add_xpath( 'direccion', "//div[@class='alojamiento-sidebar']/p[4]//text()")
		loader.add_xpath( 'servicio_turistico', "//div[@class='alojamiento-sidebar']/p[1]//text()")
		loader.add_xpath( 'url_imagen',"//ul[@class='bxslider']/li[1]/img/@src")
		#loader.add_xpath( 'latitud',"//a[@target='_new']/@href")
		#loader.add_xpath( 'longitud',"//a[@target='_new']/@href")
		loader.add_value( 'url', response.url)
		loader.add_value( 'fuente','www.chileestuyo.cl')
		loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
		loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

		yield (loader.load_item())
